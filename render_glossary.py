import yaml
import sys

def render_glossary(glossary, output=sys.stdout):
    for item in glossary:
        output.write(item['term'].strip())
        if 'symbol' in item:
            output.write(" %s"%item['symbol'].strip())
        if 'unit' in item:
            output.write(" (%s)"%item['unit'].strip())
        output.write("\n")
        output.write("~ \n%s"%item['explanation'].strip())
        output.write("\n\n")

if __name__ == '__main__':
    render_glossary(yaml.load(sys.stdin))
