attadapt.pone.noimg.pdf: attadapt.pone.noimg.tex
	pdflatex attadapt.pone.noimg
	bibtex attadapt.pone.noimg
	pdflatex attadapt.pone.noimg
	pdflatex attadapt.pone.noimg

attadapt.pone.noimg.tex: attadapt.pone.tex biblio.bib
	sed 's/^\\includegraphics/%\\includegraphics/g' < $< > $@

attadapt.pone.pdf: attadapt.pone.tex
	pdflatex attadapt.pone
	bibtex attadapt.pone
	pdflatex attadapt.pone
	pdflatex attadapt.pone

supplement.pdf: supplement.mdw
	pweave -m -f pandoc $<
	pandoc supplement.md -s -o $@

attadapt.pone.tex: attadapt.md
	pandoc $< --filter $(HOME)/.cabal/bin/pandoc-crossref -w latex --filter ./numcite.py -s -S -M template-path=templates/plos-one --template=templates/plos-one/template.latex -o $@

attadapt.html: attadapt.md biblio.bib
	pandoc $< --filter $(HOME)/.cabal/bin/pandoc-crossref --filter pandoc-citeproc --mathjax -c styling.css -V lang=en --to=html5 --smart -S -o $@

attadapt.pdf: attadapt.md biblio.bib
	pandoc $< --filter $(HOME)/.cabal/bin/pandoc-crossref --filter pandoc-citeproc --csl templates/plos-one/plos.csl --latex-engine=pdflatex -o $@

attadapt.rtf: attadapt.md biblio.bib
	pandoc -s $< --filter $(HOME)/.cabal/bin/pandoc-crossref --filter pandoc-citeproc -o $@

attadapt.docx: attadapt.md biblio.bib
	pandoc -s $< --filter $(HOME)/.cabal/bin/pandoc-crossref --filter pandoc-citeproc -o $@

attadapt.md: attadapt.mdw igiVsHeadway.py
	pweave -m -f pandoc $<
