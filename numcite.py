#!/usr/bin/env python

from pandocfilters import toJSONFilter, Str, RawInline, stringify
import sys
import pprint
import bibtexparser
from bibtexparser import customization as bparse
import copy

_bibliography=None
def get_bibliography(metadata):
    global _bibliography
    if _bibliography is not None:
        return _bibliography
    print >>sys.stderr, metadata['bibliography']
    path = metadata['bibliography']['c'][0]['c']
    _bibliography = bibtexparser.load(open(path)).get_entry_dict()
    return _bibliography

def get_author(key, metadata):
    record = bparse.author(copy.copy(get_bibliography(metadata)[key]))
    authors = record['author']
    primary = authors[0].split(',')[0]
    if len(authors) > 1:
        primary += " et al."
    return primary
    

def format_citations(citations, metadata):
    current = []
    for citation in citations:
      mode = citation['citationMode']['t']
      key = citation['citationId']
      if mode not in ['NormalCitation', 'AuthorInText']:
          raise ValueError("Can't handle citation mode %s"%mode)
      if citation['citationPrefix']:
          raise ValueError('Citation prefixes not supported!')
      if not citation["citationSuffix"] and mode == 'NormalCitation':
          current.append(key)
          continue
      if len(current) != 0:
          yield "\cite{%s}"%(','.join(current))
          current = []
      
      if citation['citationSuffix']:
        result = "\cite[%s]{%s}"%(
              stringify(citation['citationSuffix']),
              key)
      else:
        result = "\cite{%s}"%(key)
      if mode == 'AuthorInText':
          try:
            author = get_author(key, metadata)
            result = author + result
          except KeyError:
            pass
      yield result

    if len(current) != 0:
          yield "\cite{%s}"%(','.join(current))
          current = []

def caps(key, value, format, meta):
  #if format != 'latex': return
  if key != 'Cite': return
  
  citations = value[0]
  return RawInline('latex', "".join(format_citations(citations, meta)))

if __name__ == "__main__":
  toJSONFilter(caps)
