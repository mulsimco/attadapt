# Task-Difficulty Homeostasis in Car Following Models: Experimental Validation Using Self-Paced Visual Occlusion

Source code and data for the article. The main starting point is `attadapt.mdw`.

## Building

On Debian (sid at time of writing, perhaps works on derivatives too):

	git pull <repo>
	cd attadapt
	sudo apt-get install pandoc pandoc-citeproc cabal-install \
		python-seaborn python-rpy2 \
		texlive-latex-extra dvipng \
		python-pandocfilters python-bibtexparser
	sudo R -e "install.packages(c('deming','MASS','cocor'), repos='http://cran.us.r-project.org')"
	sudo pip install pweave
	cabal update
	cabal install pandoc-crossref
	make attadapt.pdf

