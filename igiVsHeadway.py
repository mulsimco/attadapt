import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
import matplotlib.pyplot as plt
import sys
import scipy.stats
from scipy.interpolate import interp1d
import statsmodels.api as sm
import statsmodels.formula.api as smf
import pandas as pd
import seaborn as sns
sns.set(style="white")
import rpy2.robjects as robj
from rpy2.robjects.packages import importr
#import rpy2.robjects.numpy2ri as numpy2ri
#numpy2ri.activate()
import rpy2.robjects.pandas2ri as pandas2ri
pandas2ri.activate()
import gzip
import json


def memoize(func):
    value = []
    def wrapper():
        if len(value) > 0:
            return value[0]
        value.append(func())
        return value[0]
    return wrapper

@memoize
def loadData():
    data = np.load(gzip.GzipFile('sessions.npyz'))
    # Due to a bug in the simulator, the timestamp is incremented twice per loop
    return data

#data = np.load(sys.stdin)
#odata = data
#data = data[data['experiment'] == 'forcedBlindFollowInTraffic']

#sids = np.unique(data['sessionId'])
#data = odata[np.in1d(odata['experiment'], ['forcedBlindFollowInTraffic', 'followInTraffic'])]

def bestlatency(t, x, y, rng=np.arange(0, 5, 0.1)):
    maxcorr = -1
    best_latency = None
    for latency in rng:
        xf = scipy.interpolate.interp1d(t - latency, x, bounds_error=False)
        shifted = xf(t)
        valid = np.isfinite(shifted)
        corr = scipy.stats.pearsonr(shifted[valid], y[valid])[0]
        if corr > maxcorr:
            maxcorr = corr
            best_latency = latency
    return best_latency

def stupidsim(ts, leaderx, dt=0.01, x=0):
    v = 0
    f = 0
    t = ts[0]
    leader_raw = scipy.interpolate.interp1d(ts, leaderx, bounds_error=False)
    def leader(t):
        x = leader_raw(t)
        if np.isnan(x):
            return leaderx[-1]
        return x
    target_hw = 10
    while x < 1000:
        hw = (leader(t) - x)/v

        hw_delta = hw - target_hw
        a = (1.0/(1 + np.exp(-hw_delta/2.0)) - 0.5)*2
        if a < 0:
            a *= 10
        else:
            a *= 3
        
        v += a*dt
        x += v*dt
        t += dt
        yield t, x, leader(t) - x, v

def headwayVsAccel():
    data = loadData()
    data = data[np.in1d(data['experiment'], ['followInTraffic'])]
    for si, sid in enumerate(np.unique(data['sessionId'])):
        sd = data[data['sessionId'] == sid]
        blocks = np.unique(sd['block'])
        blocks = blocks[-4:]
        sagg = []
        crashed = False
        for i, block in enumerate(blocks):
            bd = sd[sd['block'] == block]
            #bd = bd[bd['player'] > 5]
            if len(bd) == 0 or bd['player'][-1] < 1990:
                continue
            t = bd['absTs']
            dt = np.gradient(bd['ts'])
            playerSpeed = np.gradient(bd['player'])/dt
            leaderSpeed = np.gradient(bd['leader'])/dt
            distance = bd['leader'] - bd['player']
            angle = np.tan(2/distance)
            headway = distance/playerSpeed
            dhw = np.gradient(headway)/dt
            accel = np.gradient(playerSpeed)/dt
            latency = bestlatency(t, playerSpeed, leaderSpeed)

            def shift(signal):
                return scipy.interpolate.interp1d(t, signal, bounds_error=False)(t - latency)

            #plt.plot(t, angle/playerSpeed, '.', color='green')
            
            #plt.plot(t, playerSpeed, color='red')
            #plt.plot(t, shift(leaderSpeed))
            #plt.plot(playerSpeed, np.sqrt(shift(distance)), 'k.', alpha=0.1)
            plt.plot(playerSpeed, shift(distance)/(playerSpeed + 3), 'k.', alpha=0.1)
            #plt.plot(t, angle, '.', color='green')
            #plt.plot(t, playerSpeed/odistance, '.', color='red')
            #plt.plot(t, headway2, color='green')
            #plt.plot(t, headway, color='red')
            #plt.plot(t, leaderSpeed, color='red')
            #plt.plot(t, accel)
            plt.show()

def histogram_hdr(hist, bin_size, mass_limits):
    # Not very fast!
    ordered = np.sort(hist.flatten())[::-1]
    cum_dens = np.cumsum(ordered*bin_size)
    
    density_limits = np.searchsorted(cum_dens, mass_limits)
    invalid = density_limits >= len(ordered)
    invalid_values = [np.nan] * np.sum(invalid)
    values = ordered[density_limits[~invalid]]
    return np.array(list(values) + invalid_values)


TRIAL_DISTANCE = 2000
OMIT_DISTANCE = 300
START_DISTANCE = OMIT_DISTANCE
END_DISTANCE = TRIAL_DISTANCE - OMIT_DISTANCE
BLINK_DURATION = 0.3
MINIMUM_SPEED = 1.0


def headwayDistribution(experiments=['followInTraffic']):
    data = loadData()
    data = data[np.in1d(data['experiment'], experiments)]
    
    pow = lambda x: np.power(2, x)
    depow = np.log2
    powbase = 2

    #pow = lambda x: x
    #depow = lambda x: x
    #powbase = 1
    
    extras = []
    agg = []
    for si, sid in enumerate(np.unique(data['sessionId'])):
        sd = data[data['sessionId'] == sid]
        blocks = np.unique(sd['block'])
        blocks = blocks[-4:]
        sagg = []
        crashed = False
        for i, block in enumerate(blocks):
            bd = sd[sd['block'] == block]
            #bd = bd[bd['player'] > 5]
            if len(bd) == 0 or bd['player'][-1] < 1990:
                continue
            bd = bd[(bd['player'] > START_DISTANCE) & (bd['player'] < END_DISTANCE)]
            t = bd['absTs']
            dt = np.gradient(bd['ts'])
            playerSpeed = np.gradient(bd['player'])/dt
            distance = bd['leader'] - bd['player'] - VEHICLE_LENGTH
            #angle = np.arctan(1/distance)
            #angle_delta = np.gradient(angle)/dt
            headway = distance/playerSpeed

            valid = playerSpeed > 1
            sagg.append(np.mean(depow(headway[valid])))
            if np.isnan(sagg[-1]):
                print np.min(headway[valid])
                plt.plot(distance[valid])
                #plt.hist(headway[valid])
                plt.show()
            extras.append((np.mean(distance), np.mean(playerSpeed)))
            #plt.hist(depow(headway[valid]), bins=100)
            #plt.show()
        agg.append(np.mean(sagg))
    
    extras = np.array(extras)
    #plt.plot(extras[:,0], extras[:,1], '.')
    #plt.hist(extras[:,0])
    #plt.show()
    de = scipy.stats.gaussian_kde(agg)
    #print scipy.stats.shapiro(agg), len(agg)
    fit = scipy.stats.norm(*scipy.stats.norm.fit(agg))
    rng = np.linspace(np.min(agg) - 1, np.max(agg) + 0.5, 5000)
    dens = de(rng)
    binsize = (np.max(rng) - np.min(rng))/len(rng)
    cdens = np.cumsum(dens*binsize)
    #print pow(rng[cdens.searchsorted(0.10)])

    levels = histogram_hdr(de(rng), binsize, [0.5, 0.75])
    
    for l in levels:
        in_level = dens >= l
        plt.fill_between(pow(rng), dens, where=in_level, color='black', alpha=0.25)

    #plt.hist(pow(agg))
    #plt.hist(pow(agg), label='Histogram', normed=True, histtype='step')
    plt.plot(pow(rng), de(rng), 'k-', label='Empirical density function')
    plt.plot(pow(rng), fit.pdf(rng), label='Log-normal fit')
    plt.xlabel('Per-subject mean time gap (s)')
    plt.ylabel('Density')
    plt.legend()
    plt.show()
    
    mode = pow(scipy.optimize.minimize(lambda x: -de(x), np.median(agg)).x)
    return {
        'mode': mode,
        'shapiro': scipy.stats.shapiro(agg)
            }

def sampleTrialPlot():
    frame = getRawFeatures()
    agg = getRunAverages()
    agg = agg.sort_values('timeGap')
    ps = [0.25, 0.5, 0.75]
    nps = len(ps)
    ax = None
    for i, p in enumerate(ps):
        med = agg.iloc[int(round(len(agg)*p))]
        ax = plt.subplot(nps,1,i+1, sharex=ax, sharey=ax)
        d = frame[(frame.sessionId == med.sessionId) & (frame.block == med.block)]
        (pplot,) = plt.plot(d['ts'], d['speed']*3.6, label='Participant')
        (lplot,) = plt.plot(d['ts'], d['leaderSpeed']*3.6, 'k--', label='Leading vehicle')
    
        startT, endT = d['ts'].values[d['player'].searchsorted([START_DISTANCE, END_DISTANCE])]
        plt.axvspan(d['ts'][0], startT, color='black', alpha=0.1)
        plt.axvspan(endT, d['ts'][-1] + 1000, color='black', alpha=0.1)
    
        blinktimes = d['ts'][~np.isnan(d['blindDur'])]
        for t in blinktimes:
            blinkplot = plt.axvline(t, color='black', alpha=0.3, lw=1, ymax=0.1)
        plt.ylabel('Speed (km/h)')
        plt.xlim(d['ts'][0], d['ts'][-1])
    
    #plt.figlegend((pplot, lplot, blinkplot), ('Participant', 'Leading vehicle', 'Glance'), loc=3, mode='expand', bbox_to_anchor=(0., 1.02, 1., .102), ncol=3)
    plt.figlegend((pplot, lplot, blinkplot), ('Participant', 'Leading vehicle', 'Glance'), loc='upper left', ncol=3)
    plt.xlabel('Time (s)')
    return ps

def tgIgiSamplePlot():
    frame = getRawFeatures()
    blinks = frame[~np.isnan(frame.blindDur)]
    import scipy.signal

    def detrend(d):
            d['timeGapDetrend'] = scipy.signal.detrend(d['timeGap'])
            d['blindDurDetrend'] = scipy.signal.detrend(d['blindDur'])

            d['blindDurDetrend'] = (d['blindDur'])
            d['timeGapDetrend'] = (d['timeGap'])
            return d
    blinks = blinks.groupby(('sessionId', 'block')).apply(detrend)
    corrs = blinks.groupby(('sessionId', 'block')).apply(lambda d: scipy.stats.spearmanr(d['timeGapDetrend'], d['blindDurDetrend'])[0]).reset_index(name='corr')
    corrs.sort_values('corr', inplace=True)
    ps = [0.25, 0.5, 0.75]
    ax = None
    for i, p in enumerate(ps):
        ax = plt.subplot(3,1,i+1, sharex=ax, sharey=ax)
        idx = round(len(corrs)*p)
        key = corrs.iloc[int(idx)]
        d = blinks[(blinks.sessionId == key.sessionId) & (blinks.block == key.block)]
        #(tgPlot,) = plt.plot(d['ts'], d['timeGapDetrend'], 'o-')
        (blindDurPlot,) = plt.plot(d['ts'], d['blindDurDetrend'], 'ko--')
        if i == 1:
            plt.ylabel('Time gap / blind duration (seconds)')
    #plt.figlegend((tgPlot, blindDurPlot), ('Time gap', 'Blinder duration'), loc='upper left', ncol=2)
    #corrs = corrs.sort_values()
    plt.xlabel('Time (seconds)')
    return

    frame = getRawFeatures()
    agg = getRunAverages()
    agg = agg.sort_values('timeGap')
    ps = [0.25, 0.5, 0.75]
    nps = len(ps)
    ax = None
    for i, p in enumerate(ps):
        med = agg.iloc[int(round(len(agg)*p))]
        ax = plt.subplot(nps,1,i+1, sharex=ax, sharey=ax)
        d = frame[(frame.sessionId == med.sessionId) & (frame.block == med.block)]
        (pplot,) = plt.plot(d['ts'], d['speed']*3.6, label='Participant')
        (lplot,) = plt.plot(d['ts'], d['leaderSpeed']*3.6, 'k--', label='Leading vehicle')
    
        startT, endT = d['ts'].values[d['player'].searchsorted([START_DISTANCE, END_DISTANCE])]
        plt.axvspan(d['ts'][0], startT, color='black', alpha=0.1)
        plt.axvspan(endT, d['ts'][-1] + 1000, color='black', alpha=0.1)
    
        blinktimes = d['ts'][~np.isnan(d['blindDur'])]
        for t in blinktimes:
            blinkplot = plt.axvline(t, color='black', alpha=0.3, lw=1, ymax=0.1)
        plt.ylabel('Speed (km/h)')
        plt.xlim(d['ts'][0], d['ts'][-1])
    
    plt.figlegend((pplot, lplot, blinkplot), ('Participant', 'Leading vehicle', 'Glance'), loc=3, mode='expand', bbox_to_anchor=(0., 1.02, 1., .102), ncol=3)
    plt.xlabel('Time (s)')
    return ps

@memoize
def getRawFeatures():
    return getRawFeaturesAndStats()[0]

@memoize
def getRawFeaturesAndStats():
    data = loadData()
    data = data[np.in1d(data['experiment'], ['followInTraffic', 'blindFollowInTraffic'])]
    agg = []
    participants = json.load(open('participants.json'))
    participants = {p['sessionId']: p for p in participants}
    
    runs = []
    explevels = {
            'none': 0,
            'yearly': 6,
            'monthly': 24,
            'weekly': 104.0,
            'daily': 365.0
            }

    stats = []
    frame = pd.DataFrame(data)
    
    def dropPractices(x):
        blocks = x.block.unique()[-8:]
        return x[x.block.isin(blocks)]
    frame = frame.groupby(('sessionId')).apply(dropPractices)
    has_crashed = set()
    def calcSpeeds(x):
        sid = x['sessionId'][0]
        status = 'passed'
        if x['player'][-1] < 1900: # Crashed
                if x['player'][-1] < 10:
                    status = 'redLight'
                else:
                    status = 'crashed'
        stats.append((x['sessionId'][0], x['block'][0], x['experiment'][0], status))
        if status == 'crashed':
            has_crashed.add(sid)
        if status != 'passed':
            return x.iloc[0:0]
        #if sid in has_crashed:
        #    return None
        dt = np.gradient(x['ts'])
        x['speed'] = np.gradient(x['player'])/dt
        x['distance'] = x['leader'] - x['player'] - VEHICLE_LENGTH
        x['leaderSpeed'] = np.gradient(x['leader'])/dt
        x['timeGap'] = x['distance']/x['speed']
        x['lTimeGap'] = np.log2(x['timeGap'])
        blind = x['blind'].values.astype(np.int)
        blinkIdx = np.flatnonzero(np.diff(blind) < 0)
        bt = x['ts'].values[blinkIdx]
        blindDur = bt[1:] - bt[:-1] - BLINK_DURATION
        x['blindDur'] = np.nan
        x['blindDur'].iloc[blinkIdx[:-1]] = blindDur
        x['lBlindDur'] = np.nan
        x['lBlindDur'].iloc[blinkIdx[:-1]] = np.log2(blindDur)
        return x
    frame = frame.groupby(('sessionId', 'block')).apply(calcSpeeds)
    stats = pd.DataFrame.from_records(stats, columns='sessionId,block,experiment,status'.split(','))
    return frame, stats

@memoize
def getFeatures():
    frame = getRawFeatures()
    frame = frame[frame.speed > MINIMUM_SPEED]
    frame = frame[frame.player > START_DISTANCE]
    frame = frame[frame.player < END_DISTANCE]
    return frame

def average(x):
        #return np.nanmean(x)
        try:
            return np.exp(np.nanmean(np.log(x)))
        except AttributeError:
            return (np.nanmean(x))
        # return (np.nanmedian((x)))

@memoize
def getRawRunAverages():
    frame = getFeatures()
    agg = frame.groupby(('sessionId', 'experiment', 'block'), as_index=False).aggregate(average)
    return agg

@memoize
def getSubjectAverages():
    frame = getFeatures()
    agg = frame.groupby(('sessionId', 'experiment'), as_index=False).aggregate(average)
    ctrl = agg[agg['experiment'] == 'followInTraffic']
    agg = agg[agg['experiment'] == 'blindFollowInTraffic']
    #agg = agg.groupby(('sessionId', 'experiment')).aggregate(average).reset_index()
    agg = agg.merge(ctrl, on='sessionId', suffixes=('', '0'))
    return agg

@memoize
def getRunAverages():
    agg = getRawRunAverages()
    ctrl = agg[agg['experiment'] == 'followInTraffic'].groupby(('sessionId', 'experiment')).aggregate(average)
    ctrl = ctrl.reset_index()
    agg = agg[agg['experiment'] == 'blindFollowInTraffic']
    #agg = agg.groupby(('sessionId', 'experiment')).aggregate(average).reset_index()
    agg = agg.merge(ctrl, on='sessionId', suffixes=('', '0'))
    return agg

def igiVsHeadwayFree():
    agg = getSubjectAverages()
    #agg['timeGapDiff'] = np.log(np.exp(agg['timeGap']) - np.exp(agg['timeGap0']))
    agg['timeGapDiff'] = (agg['timeGap'] - agg['timeGap0'])
    #for g, x in agg.groupby('sessionId'):
    #    #resid = np.log(x['blindDur']) - np.log(x['timeGap'])
    #    #plt.plot(np.log(x['timeGap0']), resid, 'o')
    #    plt.plot((x['blindDur']), 1.0/(x['timeGap']), 'o')
    R = robj.r
    R.library('deming')
    model = R.deming(R("timeGapDiff ~ blindDur"), data=agg)
    print model
    print model.response(data)
    plt.plot(agg['blindDur'], agg['timeGapDiff'], '.')
    plt.show()
    
    return
    plt.show()
    r = robj.r
    models = [
            r.lm('lTimeGapDiff ~ lBlindDur + 0', data=agg),
            r.lm('lTimeGapDiff ~ lBlindDur', data=agg),
            r.lm('lTimeGapDiff ~ lBlindDur + lTimeGap0 + 0', data=agg),
            r.lm('lTimeGapDiff ~ lBlindDur + lTimeGap0', data=agg)]
    print r.summary(models[0])
    print r.anova(*models)
    return

VEHICLE_LENGTH = 4.5

def headwayVsIgi():
    frame = getFeatures()
    agg = getSubjectAverages()
    blinks = frame[~np.isnan(frame.blindDur)]
    R = robj.r
    R.library('deming')
    def detrend(d):
            t = d['ts']
            d['timeGapTrend'] = np.mean(d['timeGap'])
            d['blindDurTrend'] = np.mean(d['blindDur'])
            d['lTimeGapTrend'] = np.mean(d['lTimeGap'])
            d['lBlindDurTrend'] = np.mean(d['lBlindDur'])
            return d
    blinks = blinks.groupby(('sessionId',)).apply(detrend)
    for s, d in blinks.groupby(('sessionId',)):
        #s = g[0]
        print s
        sagg = agg[agg.sessionId == s].iloc[0]
        d['lTimeGap0'] = sagg['lTimeGap0']
        #d['timeGap'] -= sagg['timeGap0']
        #model = R.lm('lBlindDur ~ lTimeGap + lTimeGap0', data=d)
        fit = R.deming(R.formula('blindDur ~ timeGap'), data=d)
        fit = np.poly1d(list(R.coef(fit))[::-1])

        intercept = fit[0]
        #resid = d['lTimeGapTrend'] - d['lBlindDurTrend']
        plt.plot(sagg['lTimeGap0'], intercept, 'o')
        m, s = np.mean(d['timeGap']), np.std(d['timeGap'])
        rng = np.linspace(m - s, m + s, 100)
        #plt.plot(d['timeGap'], d['blindDur'], '.')
        #plt.plot(rng, fit(rng))
    plt.show()
    return
    for s, d in blinks.groupby(('sessionId')):
        sagg = agg[agg.sessionId == s].iloc[0]
        d['x'] = (d['timeGap']) - d['timeGapTrend']# - sagg['timeGap0'] #- d['timeGapTrend']
        d['y'] = (d['blindDur']) - d['blindDurationTrend']
        #fit = R.lm(R.formula('y ~ x'), data=d)
        fit = R.pbreg(R.formula('y ~ x'), data=d)
        fit = np.poly1d(list(R.coef(fit))[::-1])
        print fit
        m, s = np.mean(d['x']), np.std(d['x'])
        rng = np.linspace(m - s, m + s, 100)
        rrng = pd.DataFrame({'timeGap': rng})
        #plt.plot(d['x'], d['y'], '.')
        plt.scatter(np.mean(d['x']), np.mean(d['y']))
        #pred = R.predict(fit, newdata=rrng)
        pred = fit(rng)
        #plt.plot(rng, np.array(pred), color='black', alpha=0.1)
        resid = fit(d['x']) - d['x']
        #plt.plot(d['x'], resid)
        mresid = np.mean(d['x']) - np.mean(d['y'])
        #plt.plot(np.log(sagg['timeGap0']), mresid, 'o')
        #plt.scatter(sagg['timeGap'], 1.0/sagg['blindDur'], color='red')
        #plt.plot([0, 15], [0, 15])
        #plt.xlim([0, 15])
        #plt.ylim([0, 15])
        #plt.plot(d['ts'], d['blindDur'])
        #plt.show()
    plt.xlabel(r'Per-subject slope in $g(t) = \beta T(t)$')
    plt.show()

    return
    data = loadData()
    data = data[np.in1d(data['experiment'], ['followInTraffic', 'blindFollowInTraffic'])]
    agg = []
    
    pow = lambda x: np.power(2, x)
    depow = np.log2
    powbase = 2
    
    #pow = lambda x: np.power(np.power(2, x), -1)
    #depow = lambda x: np.log2(np.power(x, -1))

    #pow = lambda x: x
    #depow = lambda x: x
    #powbase = 1
    correlations = []
    r = robj.r

    for si, sid in enumerate(np.unique(data['sessionId'])):
        sd = data[data['sessionId'] == sid]
        blocks = np.unique(sd['block'])
        blocks = blocks[-8:]
        igis = []
        hws = []
        trials = []
        times = []
        freehws = []
        crashed = False
        for i, block in enumerate(blocks):
            bd = sd[sd['block'] == block]
            if bd['player'][-1] < 10:
                continue

            if bd['player'][-1] < 1900:
                if bd['experiment'][0] == 'blindFollowInTraffic':
                    crashed = True
                continue
            bd = bd[(bd['player'] > 300) & (bd['player'] < 1700)]
            blind = bd['blind'].astype(np.int)
            blinktimes = bd['absTs'][np.diff(blind) > 0]
            igi = np.diff(blinktimes) - BLINK_DURATION
            blinktimes = blinktimes[:-1]
            if len(igi) == 0:
                migi = np.nan
            else:
                migi = np.mean(depow(igi))
            t = bd['absTs']
            dt = np.gradient(bd['ts'])
            playerSpeed = scipy.interpolate.interp1d(t, np.gradient(bd['player'])/dt)
            distance = scipy.interpolate.interp1d(t, bd['leader'] - bd['player'] - VEHICLE_LENGTH)
            headway = lambda t: (distance(t)/playerSpeed(t))
            
            if bd['experiment'][0] == 'followInTraffic':
                headways = headway(t)
                valid = playerSpeed(t) > 1.0
                freehws.extend(headways[valid])
                continue

            blinkHeadways = headway(blinktimes)
            valid = playerSpeed(blinktimes) > 1.0
            t = blinktimes[valid]
            x = depow(blinkHeadways[valid])
            y = depow(igi[valid])
            #rng = np.linspace(np.mean(x) - np.std(x), np.mean(x) + np.std(x), 100)
            times.extend(t)
            hws.extend(x)
            igis.extend(y)
            trials.extend([i]*len(y))
            fit = np.poly1d(np.polyfit(x, y, 1))
            rng = np.linspace(np.mean(x) - np.std(x), np.mean(x) + np.std(x), 100)
            """
            plt.subplot(1,2,1)
            plt.plot(blinktimes[valid], (x), 'o-', color='red')
            plt.twinx()
            plt.plot(blinktimes[valid], (y), 'o-', color='green')
            plt.subplot(1,2,2)
            plt.title("Corr: %.2f"%(scipy.stats.spearmanr(x, y)[0]))
            plt.scatter(x, y)
            plt.show()
            plt.clf()
            """
            #x = np.array(r.ar(np.log2(x), False, 1).rx2('resid')[1:])
            #y = np.array(r.ar(np.log2(y), False, 1).rx2('resid')[1:])
            #c = scipy.stats.spearmanr(x, y)[0]
            xtrend = scipy.interpolate.UnivariateSpline(t, x, s=len(t))(t)
            ytrend = scipy.interpolate.UnivariateSpline(t, y, s=len(t))(t)
            """
            plt.subplot(2,1,1)
            plt.plot(t, x - xtrend, 'r-')
            plt.plot(t, y - ytrend, 'g-')
            plt.subplot(2,1,2)
            plt.plot(t, xtrend, 'r--')
            plt.plot(t, x, 'r.')
            plt.plot(t, ytrend, 'g--')
            plt.plot(t, y, 'g.')
            plt.show()
            plt.scatter(x - xtrend, y - ytrend)
            plt.show()
            """
            c = scipy.stats.spearmanr(x - xtrend, y - ytrend)[0]
            correlations.append((c, (t, x - xtrend, y - ytrend, block, sid)))
            
            #frame = pd.DataFrame({'hw': np.log2(x), 'igi': np.log2(y), 'v': np.log(playerSpeed(blinktimes)[valid])})
            #model = r.lm('igi ~ hw + v', data=frame)
            #print r.summary(model)
            #plt.scatter(np.log2(x), np.log2(y))
            #plt.show()
            #plt.plot(pow(x), pow(y), '.')
            #plt.plot(pow(rng), pow(fit(rng)), 'k-', alpha=0.3)
            #plt.plot(depow(blinkHeadways[valid]), depow(igi[valid]), '.')
            #print scipy.stats.pearsonr(depow(blinkHeadways[valid]), depow(igi[valid]))
            #sagg.
        
        x = np.array(hws)
        y = np.array(igis)
        mhw = np.power(2, np.mean(np.log2(freehws)))
        fit = np.poly1d(np.polyfit(x, y, 1))
        rng = np.linspace(np.mean(x) - np.std(x), np.mean(x) + np.std(x), 100)
        #plt.plot(pow(rng), pow(fit(rng)), 'k-', alpha=0.3)
        for x, y, t, tr in zip(hws, igis, times, trials):
            agg.append((x, y, t, tr, mhw, si))
        #plt.show()

        #plt.plot(mhw, np.mean(nz.T[2]), 'o')
    #plt.show()
    #return
    
    correlations.sort(key=lambda x: x[0])
    n = len(correlations)
    #sns.set_style('ticks')
    percentiles = [0.9, 0.5, 0.1]
    for nth, i in enumerate((np.array(percentiles)*n).astype(np.int)):
        c, (t, hw, igi, block, si) = correlations[i]
        #plt.title(c)
        #plt.scatter(hw, igi)
        #plt.show()
        #continue
        plt.subplot(3,1,nth+1)
        t = t - t[0]
        plt.title("%i:th percentile (Spearman R = %.2f)"%(percentiles[nth]*100, c))
        x = pow(hw)
        (hwplt,) = plt.plot(t, x, '.-')
        plt.semilogy(basey=2)
        m, std = np.mean(x), np.std(x)
        plt.ylim(m - 2.5*std, m + 2.5*std)
        if nth == 1:
            plt.ylabel('Time gap (relative to baseline)')
        if nth != len(percentiles) - 1:
            plt.tick_params(
                axis='x',
                which='both',
                bottom='off',
                top='off',
                labelbottom='off')
        else:
            plt.tick_params(axis='x', which='both', top='off')
        plt.twinx()
        x = pow(igi)
        plt.semilogy(basey=2)
        m, std = np.mean(x), np.std(x)
        (igiplt,) = plt.plot(t, x, 'k.--')
        if nth == 1:
            plt.ylabel('Blinder duration (relative to baseline)')
        plt.ylim(m - 2.5*std, m + 2.5*std)
    plt.figlegend((hwplt, igiplt), ('Time gap', 'Blinder duration'), 'upper left')
    
    #for nth, i in enumerate(range(len(correlations))):
    #    c, (t, hw, igi, si) = correlations[i]
    #    plt.plot(hw, igi, '.')
    #    plt.show()

    correlations = pd.DataFrame.from_records([(c, x[-1], x[-2]) for (c, x) in correlations], columns=['correlation', 'sessionId', 'block'])
    
    
    n, s = len(correlations), (correlations['correlation'] > 0).sum()
    binom_all = s, n, scipy.stats.binom_test(s, n)
    
    medians = correlations.groupby('sessionId')['correlation'].median()
    n, s = len(medians), (medians > 0).sum()
    binom_subjects = s, n, scipy.stats.binom_test(s, n)
    
    return {
            'correlations': correlations
            }
    #plt.hist(medians, bins=5, histtype='step', normed=True)
    #plt.hist(correlations['correlation'], bins=20, histtype='step', normed=True)
    #plt.show()

def analyzeCrashes():
    frame, stats = getRawFeaturesAndStats()
    agg = getRawRunAverages()
    agg = agg.groupby(('sessionId', 'experiment')).mean()
    #print stats
    stats = pd.concat((stats, pd.get_dummies(stats['status'])), axis=1)
    mstats = stats.groupby(('sessionId', 'experiment')).mean()
    mstats = pd.concat([mstats, agg], axis=1)

    normal = mstats.xs('followInTraffic', level='experiment')
    blind = mstats.xs('blindFollowInTraffic', level='experiment')
    plt.scatter(blind.blindDur - blind.timeGap, blind['crashed'])
    #for row in stats:
    #    print row
    #print mstats['blindFollowInTraffic']
    #nstats = stats[stats.experiment = 'followInTraffic']
    

    #print stats[stats.experiment == 'followInTraffic']['crashed'].mean(), stats[stats.experiment == 'blindFollowInTraffic']['crashed'].mean()
    #stats = frame.pivot_table(index=['sessionId', 'experiment', 'block'], columns=['status'])
    #print stats


def speedVsHeadway():
    data = getFeatures()
    data = data[data.experiment == 'followInTraffic']
    print data.columns
    for sid, d in data.groupby('sessionId'):
        plt.plot(np.log(d['speed']), np.log(d['timeGap']), '.')
        plt.show()

import datetime
def drivingLicenseYears():
    participants = json.load(open('participants.json'))
    licenseyears = {}
    for p in participants:
        if p.get('drivinglicenseyear', '') == '':
            licenseyears[p['sessionId']] = 0.0
            continue
        license_date = datetime.datetime(int(p['drivinglicenseyear']), 7, 1)
        date = datetime.datetime.strptime(p['date'], '%Y-%m-%d')
        licenseyears[p['sessionId']] = ((date - license_date).days/365.0)

    return licenseyears

if __name__ == '__main__':
        #print headwayDistribution(experiments=['blindFollowInTraffic'])
        #igiVsHeadwayFree()
        #headwayVsIgi()
        #tgIgiSamplePlot()
        #analyzeCrashes()
        #sampleTrialPlot()
        #speedVsHeadway()
        #plt.show()
        drivingLicenseYears()
